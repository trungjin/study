const express = require('express');
const app = express();
const mongoose = require('mongoose');
const authRouter = require('./routes/auth');
var bodyParser = require('body-parser')
require('dotenv').config();

const connectDB = async() =>{
    try {
        await mongoose.connect('mongodb://localhost:27017/study', {
            useCreateIndex: true,
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        })

        console.log('MongoDB conenct')
    } catch (error) {
        console.log(error);
        process.exit(1)
    }
}

connectDB();

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use('/api/auth', authRouter)

const PORT = process.env.PORT || 3001;

app.listen(PORT, () => console.log(`Server started PORT ${PORT}`))