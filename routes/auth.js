const express = require('express');
const router = express.Router()
const argon2 = require('argon2');
const jwt = require('jsonwebtoken');

const User = require('../models/User');

router.get('/', (req, res) => {
    return res.send('User router');
})

router.post('/register', async(req, res) =>{
    const {username, password} = req.body;

    if(!username || !password){
        res.status(400).json({
            success: false,
            message: 'Tên đăng nhập hoặc mật khẩu không được bỏ trống'
        })
    }

    try {
        const user = await User.findOne({username});
        if(user) return res.status(400).json({
            success: false,
            message: 'Tài khoản đã tồn tại trên hệ thống'
        })
        const hashedPassword = await argon2.hash(password);
        const newUser = new User({
            username,
            password: hashedPassword
        })
        await newUser.save();
        const accessToken = jwt.sign({
            userId: newUser._id,
        }, process.env.ACCESS_TOKEN_SECRET)
        return res.json({
            success: true,
            accessToken
        })
    } catch (error) {
        
    }

    // return res.json({
    //     success: true,
    //     username,
    //     password
    // })
})

module.exports = router;